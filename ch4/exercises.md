Part I

1. Write safe versions of functions, returning Maybe:

    * `safeHead`
    * `safeTail`
    * `safeLast`
    * `safeInit`

2. Write a function `splitWith` that acts similar to words but takes a predicate and a
   list of any type, and then splits its input list on every element for which the
   predicate returns `False`:

    ```haskell
    splitWith :: (a -> Bool) -> [a] -> [[a]]
    ```

3. Write a program that prints the first word of each line of its input using the
   application framework described at beggining of chapter.
4. Write a program that transposes the text in a file. For instance, it should convert
   `"hello\nworld\n" to "hw\neo\nlr\nll\nod\n"`.

Part II

1. Use a fold (choosing the appropriate fold will make your code much simpler) to
   rewrite and improve upon the `asInt` function from the earlier section:

    ```haskell
    import Data.Char (digitToInt)

    asInt :: String -> Int
    asInt xs = loop 0 xs
      where loop acc [] = acc
            loop acc (y:ys) = let acc' = acc * 10 + digitToInt y
                              in loop acc' ys
    ```
2. Make it work for negative numbers:

    ```text
    \> asInt_fold "101"
    101
    \> asInt_fold "-31337"
    -31337
    ```
3. Extend your function to handle the following kinds of exceptional conditions by
   calling `error`:

   ```text
   \> asInt_fold ""
   0
   \> asInt_fold "-"
   0
   \> asInt_fold "-3"
   -3
   \> asInt_fold "2.7"
   *** Exception: Char.digitToInt: not a digit '.'
   \> asInt_fold "314159265358979323846"
   564616105916946374
   ```
4. When using `error` in `asInt_fold` its callers cannot handle the errors. Rewrite to
   fix:

   ```text
   type ErrorMessage = String
   asInt_either :: String -> Either ErrorMessage Int
   \> asInt_either "33"
   Right 33
   \> asInt_either "foo"
   Left "non-digit 'o'"
   ```
5. (describes `concat`)
6. Write own `concat` using `foldr`
7. Write own `takeWhile` both by using explicit recursion and also using `foldr`.
8. (describes `groupBy`)
9. Write own `groupBy` using a fold.
10. How many of the following can you rewrite using list folds?

    * any
    * cycle
    * words
    * unlines

    For those where you can use either `foldl'` or `foldr`, which is more appropriate in
    each case?
