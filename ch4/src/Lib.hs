module Lib where

import Data.Char (digitToInt, isDigit)
import Data.List (foldl', transpose, init, last)
import Data.Maybe (mapMaybe)


-- (I.1)

safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:_) = Just x

safeTail :: [a] -> Maybe [a]
safeTail [] = Nothing
safeTail (_:xs) = Just xs

safeLast :: [a] -> Maybe a
safeLast = foldr (const . Just) Nothing

safeInit :: [a] -> Maybe [a]
safeInit [] = Nothing
safeInit [_] = Just []
safeInit (x:xs) = (x :) <$> safeInit xs

-- (I.2)

splitWith :: (a -> Bool) -> [a] -> [[a]]
splitWith _ [] = []
splitWith p xs = case span p xs of
                   ([], _:zs) -> splitWith p zs
                   (ys, _:zs) -> ys : splitWith p zs
                   (ys', []) -> [ys']

-- (I.3)

firstWords :: String -> String
firstWords = unlines . fmap (headOfListsOrEmpty . words) . lines

headOfListsOrEmpty :: [[a]] -> [a]
headOfListsOrEmpty [] = []
headOfListsOrEmpty (x:_) = x

-- (I.4)

transposeLines :: String -> String
transposeLines = unlines . transpose . lines

transposeLines' :: String -> String
transposeLines' = unlines . go . lines
  where
    go :: [[a]] -> [[a]]
    go [] = []
    go l = let heads = mapMaybe safeHead l
               tails = mapMaybe safeTail l
           in if null heads
              then tails
              else heads:tails

-- (II.1)

asInt :: String -> Int
asInt xs = loop 0 xs
  where loop acc [] = acc
        loop acc (y:ys) = let acc' = acc * 10 + digitToInt y
                          in loop acc' ys

asIntFold :: String -> Int
asIntFold = foldl' (\acc x -> acc * 10 + digitToInt x) 0

-- (II.2)

asIntNegatives :: String -> Int
asIntNegatives "" = 0
asIntNegatives ('-':cs) = negate . asIntFold $ cs
asIntNegatives cs = asIntFold cs

-- (II.3)

asIntErrors :: String -> Int
asIntErrors str = case str of
  "" -> errorWithoutStackTrace "empty string"
  "-" -> errorWithoutStackTrace "just a minus. minus what?"
  ('-':cs) -> negate . asIntChecked $ cs
  cs -> asIntChecked cs
  where asIntChecked cs' | '.' `elem` cs' = errorWithoutStackTrace "make digits, not dots"
                         | otherwise = asIntFold cs'

-- (II.4)

type ErrorMessage = String
asIntEither :: String -> Either ErrorMessage Int
asIntEither str = case str of
  "" -> Left "empty string"
  "-" -> Left "just a minus. minus what?"
  ('-':cs) -> negate <$> asIntChecked cs
  cs -> asIntChecked cs
  where
    nondigits = filter (not . isDigit)
    asIntChecked cs' | null (nondigits cs') = Right $ asIntFold cs'
                     | otherwise            =
                         Left $ "non-digit " ++ show (head $ nondigits cs')

-- (II.5+6)

myConcat :: [[a]] -> [a]
myConcat = foldr (++) []

-- (II.7)

myTakeWhile1 :: (a -> Bool) -> [a] -> [a]
myTakeWhile1 _ [] = []
myTakeWhile1 p (x:xs) = if p x
                        then x:myTakeWhile1 p xs
                        else []

myTakeWhile2 :: (a -> Bool) -> [a] -> [a]
myTakeWhile2 p = foldr (\x acc -> if p x then x:acc else []) []

-- (II.8+9)

-- note this doesn't work like groupBy when using /=
-- \> groupBy (/=) [2,2,2,1,3,1,3,3,3]
-- [[2],[2],[2,1,3,1,3,3,3]]
-- \> myGroupBy (/=) [2,2,2,1,3,1,3,3,3]
-- [[2],[2],[2,1,3,1,3],[3],[3]]
-- due to right fold...

myGroupBy :: (a -> a -> Bool) -> [a] -> [[a]]
myGroupBy cmp = foldr step []
  where step x [] = [[x]]
        step x (ys@(y:_):yss) = if x `cmp` y
                                then (x:ys):yss
                                else [x]:ys:yss
        step _ _ = error "Should not have been able to construct an empty [] in our list"

-- (II.10)
myAnyR :: (a -> Bool) -> [a] -> Bool
myAnyR p = foldr ((||) . p) False

myAnyL :: (a -> Bool) -> [a] -> Bool
myAnyL p = foldl' (\acc x -> acc || p x) False

-- not quite; would need to maintain state or manipulate the result
-- \> myWordsL "  ala  baala "
-- ["ala","","baala",""]

myWordsL :: String -> [String]
myWordsL = foldl' step []
  where
    step [] ' ' = []
    step [] c = [[c]]
    step [w] ' ' = [w, []]
    step [w] c = [w ++ [c]]
    step ws  ' ' = ws ++ [[]]
    step ws c = init ws ++ [last ws ++ [c]]

myWordsL' :: String -> [[String]]
myWordsL' = scanl step [[]]
  where
    step ws ' ' = ws ++ [[]]
    step (w:ws) c = init ws ++ [last ws ++ [c]]


-- still not quite right:
-- \> myWordsR "  ala  baala "
-- ["","ala","baala"]

myWordsR :: String -> [String]
myWordsR = foldr step []
  where
    step ' ' [] = []
    step c [] = [[c]]
    step ' ' ([]:ws) = []:ws
    step ' ' (w:ws) = []:w:ws
    step c (w:ws) = (c:w):ws


myWordsR' :: String -> [(Char, String)]
myWordsR' = scanr step ('?', "")
  where
    step c (_, xs) = (c, xs)


--

pprint :: Show a => [a] -> IO ()
pprint = mapM_ print

