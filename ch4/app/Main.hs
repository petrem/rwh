module Main (main) where

import System.Environment (getArgs)

import Lib

main :: IO ()
main = mainWith myFunction
  where mainWith function = do
          args <- getArgs
          case args of
            [input, output] -> interactWith function input output
            _ -> putStrLn "error: exactly two arguments needed"
        myFunction = firstWords


interactWith :: (String -> String) -> String -> String -> IO ()
interactWith function inputFile outputFile = do
  input <- readFile inputFile
  let output = function input
  case outputFile of
    "-" -> putStrLn output
    _   -> writeFile outputFile output
