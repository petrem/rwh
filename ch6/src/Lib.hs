module Lib where

import Data.List (intercalate)


someFunc :: IO ()
someFunc = putStrLn "someFunc"

-- similar to how Show works for strings vs plain lists

class Fooable a where
  foo :: a -> String
  fooList :: [a] -> String
  fooList as = "["++ (intercalate "," . map foo) as ++ "]"

instance Fooable Char where
  foo _ = "Char"
  fooList _ = "CharList"

instance Fooable Int where
  foo _ = "Int"

-- instance Fooable [Char] where
--   foo _ = "String"
