module Queens where

-- this isn't a RWH exercise, but I found a reference to Turner's paper while snooping
-- around the "to read" list

-- the board is represented as a list, each element is the rank of an occupying queen
-- on the files in order from A to H.
-- e.g. [] is the empty board, and [2,5,4] is Qa2, Qb5, Qc4.

import Data.List (singleton, permutations)
import qualified Data.List as L
import Data.Function (on)
import Data.Monoid (Any(..))


newtype Board = Board [Int] deriving Eq

instance Show Board where
  show (Board qs) = unwords positions
    where
      positions = foldr (\(file, rank) acc -> ("Q" ++ singleton file ++ show rank):acc) [] $
                  zip ['a'..'h'] qs

-- this is an adaptation of Turner's implementation in KRC
queens :: [Board]
queens =  Board <$> go 8
  where
    go 0 = [[]]
    go n = [q:bs | q <- [1..8], bs <- go (n-1), safe q bs]

    safe q = not . getAny . foldMap (Any . checks q) . zip [1..]

    checks q (i, b) = q == b || abs (q - b) == i

-- queens' :: [Board]
-- queens' = Board <$> filter (not.unsafe) rooks
--   where
--     rooks = permutations [1..8]
--     unsafe = ... -- check diagonals here

combinationsOf :: Int -> [a] -> [[a]]
combinationsOf n xs = let l = length xs
                      in if n > l
                         then []
                         else subsequencesBySize xs !! (l-n)

subsequencesBySize [] = [[[]]]
subsequencesBySize (x:xs') = let next = subsequencesBySize xs'
                             in zipWith (++) ([]:next) (map (map (x:)) next ++ [[]])

-- this is so much faster (in ghci; does it hold after optimizations?)
queens' :: [Board]
queens' = Board <$> go 8
  where
    go 0 = [[]]
    go n = filter safe $ (:) <$> [1..8] <*> go (n-1)
    safe [] = True
    safe (q:bs) = not . getAny . foldMap (Any . checks q) . zip [1..] $ bs
    checks q (i, b) = q == b || abs (q - b) == i
