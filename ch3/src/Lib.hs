module Lib where

import Data.List (sortBy)


someFunc :: IO ()
someFunc = putStrLn "someFunc"

-- ch2; pretend we don't yet know about patterns, list library, etc
myLast :: [a] -> a
myLast xs = if null xs
            then error "Empty list"
            else if null (tail xs)
                 then head xs
                 else myLast (tail xs)

myLastButOne :: [a] -> a
myLastButOne xs = if null xs
                     then error "Empty list"
                     else if null (tail xs)
                          then error "Just one element, still not good"
                          else if null (tail (tail xs))
                               then head xs
                               else myLastButOne (tail xs)


-- ch3

data MyList a = Cons a (MyList a) | Nil deriving (Eq, Show)

toMyList :: [a] -> MyList a
toMyList [] = Nil
toMyList (x:xs) = Cons x (toMyList xs)

fromMyList :: MyList a -> [a]
fromMyList Nil = []
fromMyList (Cons x xs) = x:fromMyList xs

myLength :: [a] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

myMean :: Integral a => [a] -> Double
myMean [] = 0
myMean xs = let (s, l) = sumNLength 0 0 xs
            in fromIntegral s / l
  where sumNLength acc_s acc_l [] = (acc_s, acc_l)
        sumNLength acc_s acc_l (y:ys) = sumNLength (acc_s+y) (acc_l+1) ys

data Tree a = Node (Maybe (Tree a)) a (Maybe (Tree a)) deriving (Eq, Show)

simpleTree = Node (Just (Node Nothing 1 Nothing)) 2 (Just (Node Nothing 3 Nothing))

mirror :: [a] -> [a]
mirror [] = []
mirror (x:xs) = x:mirror xs ++ [x]

isPalindrome :: Eq a => [a] -> Bool
isPalindrome [] = True
isPalindrome [_] = True
isPalindrome (x:xs) = x == last xs && isPalindrome (init xs)

isPalindrome' :: Eq a => [a] -> Bool
isPalindrome' lst = go [] lst
  where go rs [] = rs == lst
        go rs (x:xs) = go (x:rs) xs


sortByLength :: [[a]] -> [[a]]
sortByLength = sortBy lengthCompare
  where lengthCompare xs ys = compare (length xs) (length ys)

myIntersperse :: a -> [[a]] -> [a]
myIntersperse x (xs:xs':xss) = xs ++ [x] ++ xs' ++ myIntersperse x xss
myIntersperse _ [xs] = xs
myIntersperse _ _ = []

myIntersperse' :: a -> [[a]] -> [a]
myIntersperse' x xss = foldr go [] xss
  where go xs ys = undefined

-- determine height of binary tree
data BinTree a = BNode (BinTree a) a (BinTree a) | BEmpty

simpleBinTree = BNode (BNode BEmpty 1 BEmpty) 2 (BNode BEmpty 3 BEmpty)

binTreeHeight :: BinTree a -> Int
binTreeHeight BEmpty = 0
binTreeHeight (BNode l _ r) = 1 + max (binTreeHeight l) (binTreeHeight r)
