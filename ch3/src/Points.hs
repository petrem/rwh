{-# LANGUAGE OverloadedStrings #-}

module Points where

import Data.Function (on)
import Data.List (groupBy, minimumBy, maximumBy, sortOn, sortBy)
import Data.Text (Text)
import qualified Data.Text as T


class Displayable a where
  label :: a -> Text

-- three two-dimensional points a, b, c. looking from a through b at the angle formed by ab with bc, it can turn left or right (or straight); define Direction data type that lets you represent these possibilities
data Point a = Point { coordX :: a
                     , coordY :: a
                     , pLabel :: Text
                     } deriving (Eq, Show)

instance Displayable (Point a) where
  label = pLabel

-- newtype OrdYPoint a = OrdYPoint {getOrdYPoint :: (Point a)} deriving (Eq, Show)
-- newtype OrdXPoint a = OrdXPoint {getOrdXPoint :: (Point a)} deriving (Eq, Show)

-- instance Ord a => Ord (OrdYPoint a) where
--   compare = compare `on` (coordY . getOrdYPoint)

-- instance Ord a => Ord (OrdXPoint a) where
--   compare = compare `on` (coordY . getOrdXPoint)


data Segment a = Segment { segStart :: Point a
                         , segEnd :: Point a
                         } deriving (Eq, Show)

data Orientation = ToLeft | Straight | ToRight deriving (Eq, Show)


rise :: (Real a, Fractional b) => Point a -> Point a -> b
rise = (realToFrac .) . ((-) `on` coordY)


run :: (Real a, Fractional b) => Point a -> Point a -> b
run = (realToFrac .) . (-) `on` coordX


slope :: (Real a, Fractional b) => Point a -> Point a -> b
slope a b = rise b a / run b a


squaredDistance :: (Real a, Fractional b) => Point a -> Point a -> b
squaredDistance a b = run a b ^^ (2::Int) + rise a b ^^ (2::Int)


-- | Calculate the turn made by three points
orientation :: (Real a) => Point a -> Point a -> Point a -> Orientation
orientation (Point x0 y0 _) (Point x1 y1 _) (Point x2 y2 _)
  | slopeDiff > 0 = ToRight
  | slopeDiff < 0 = ToLeft
  | otherwise     = Straight
  where slopeDiff = ((y1 - y0) * (x2 - x1)) - ((x1 - x0) * (y2 - y1))


-- angleFromOx :: (Real a, Fractional b) => Point a -> Point a -> b
-- angleFromOx a b | run == 0 = Vertical
--                 | otherwise = Slope $ toRational rise / toRational run
--   where rise = coordY b - coordY a
--         run = coordX b - coordX a


isOnSegment :: (Real a) => Segment a -> Point a -> Bool
isOnSegment (Segment (Point px py _) (Point rx ry _)) (Point qx qy _) =
     qx >= min px rx
  && qx <= max px rx
  && qy >= min py ry
  && qy <= max py ry


-- | Compute the direction of each successive triple:
-- | [a..e] -> [turn [a..c], turn [b..d] .. turn [c..e]]
orientations :: (Real a) => [Point a] -> [Orientation]
orientations [] = []
orientations [_] = []
orientations [_, _] = []
orientations (p1:p2:p3:ps) = orientation p1 p2 p3 : orientations (p2:p3:ps)


areIntersecting :: (Real a) => Segment a -> Segment a -> Bool
areIntersecting s1@(Segment p1 p2) s2@(Segment p3 p4)
  | dir_s1_p3 /= dir_s1_p4 && dir_s2_p1 /= dir_s2_p2 = True
  | dir_s1_p3 == Straight = isOnSegment s1 p3
  | dir_s2_p1 == Straight = isOnSegment s2 p1
  | dir_s2_p2 == Straight = isOnSegment s2 p2
  | otherwise = False
  where dir_s1_p3 = orientation p1 p2 p3
        dir_s1_p4 = orientation p1 p2 p4
        dir_s2_p1 = orientation p3 p4 p1
        dir_s2_p2 = orientation p3 p4 p2

newtype Polygon a = Polygon {runPolygon :: [Point a]} deriving (Eq, Show)


-- | Check if point is inside polygon; assumes a convex polygon.
isInPolygon :: (Real a) => Polygon a -> Point a -> Bool
isInPolygon (Polygon ps) p = and $ any <$> [isToLeft, isToRight, isAbove, isBelow] <*> [ps]
  where isToLeft = (coordX p >=) . coordX
        isToRight = (coordX p <=) . coordX
        isAbove = (coordY p <=) . coordY
        isBelow = (coordY p >=) . coordY


isConvexPolygon :: (Real a) => Polygon a -> Bool
isConvexPolygon (Polygon []) = True
isConvexPolygon (Polygon ps) =    all (== ToRight) (orientations psAndBack)
                               || all (== ToLeft) (orientations psAndBack)
  where psAndBack = ps ++ [head ps]

-- | compare two points by distance from the first
distanceComparator :: Real a => Point a -> Point a -> Point a -> Ordering
distanceComparator p0 = compare `on` squaredDistance p0

-- strictOrdering :: Ordering -> Ordering
-- strictOrdering EQ = LT
-- strictOrdering o = o

-- -- | as distanceComparator but 
-- distanceComparator' :: Point a -> Point a -> Point a -> Ordering
-- distanceComparator' p0 = strictOrdering . (compare `on` squaredDistance p0)

compareByPolarAngle :: Real a => Point a -> Point a -> Point a -> Ordering
compareByPolarAngle p0 p1 p2 =
  case orientation p0 p1 p2 of
    Straight -> distanceComparator p0 p1 p2
    ToLeft -> LT
    ToRight -> GT


-- | Graham's scan algorithm for the convex hull of 2d points.
-- | See en.wikipedia.org/wiki/Convex_hull and wiki/Graham_scan
-- | See Cormen&al, Introduction to algorithms, 3rd edition, pp. 1030
convexHullGraham :: (Real a) => [Point a] -> Polygon a
convexHullGraham points@(_:_:_) = checkAndCompute p0 candidates
  where
    p0 = bottomLeftMostPoint points
    -- filter out the initial point, sort by angle with p0 (using slope as proxy for
    -- the angle magnitude; note that these angles must be in quadrant I or II
    -- due to how p0 was picked) then eliminate co-linear points except the farthest
    sortKey = (`slope` p0)
    --sortedPointsAroundP0 = sortOn sortKey (filter (/= p0) points)
    sortedPointsAroundP0 = sortBy (compareByPolarAngle p0)(filter (/= p0) points)
    candidates =
      maximumBy (distanceComparator p0) <$>
      groupBy ((==) `on` sortKey) sortedPointsAroundP0

    checkAndCompute c0 (c1:c2:cs) = Polygon $ scanPoints [c2,c1,c0] cs
    checkAndCompute _ _ = error "Convex hull is empty"

    scanPoints stack [] = stack
    scanPoints stack@(s1:s0:ss) (p:ps)
      | orientation s0 s1 p == ToLeft = scanPoints (p:stack) ps
      | otherwise                     = scanPoints (s0:ss) (p:ps)
    scanPoints _ _ = error "Should not be reached"
convexHullGraham _ = error "Not enough points"


bottomLeftMostPoint :: Real a => [Point a] -> Point a
bottomLeftMostPoint = minimumBy ((compare `on` coordY) <> (compare `on` coordX))


-- test data

somePoints :: [Point Int]
somePoints = [ Point 0 3 "A"
             , Point 1 1 "B"
             , Point 2 2 "C"
             , Point 4 4 "D"
             , Point 0 0 "E"
             , Point 1 2 "F"
             , Point 3 1 "G"
             , Point 1 0 "H"
             , Point 3 3 "I"
             ]

expectedHull :: [Point Int]
expectedHull = [ Point 0 0 "E"
               , Point 1 0 "H"
               , Point 3 1 "G"
               , Point 4 4 "D"
               , Point 0 3 "A"
               ]


-- Display things

data Row a = Row { rowNumber :: a
               , rowData :: [Text]
               } deriving (Show, Eq)

instance Displayable (Row a) where
  label = T.intercalate " " . rowData

asciiArtShowPoints :: (Integral a, Show a) => [Point a] -> Text
asciiArtShowPoints ps = T.unlines . reverse $ rez
  where
    minX = coordX $ minimumBy (compare `on` coordX) ps
    maxX = coordX $ maximumBy (compare `on` coordX) ps
    minY = coordY $ minimumBy (compare `on` coordY) ps
    maxY = coordY $ maximumBy (compare `on` coordY) ps
    width = maxX - minX + 1
    fromBottomLeftToTopRight =
      sortBy ((compare `on` coordY) <> (compare `on` coordX)) ps
    byLine = labeledGroupOn1 coordY fromBottomLeftToTopRight
    withLineGapsFilled = fillGaps "." minX maxX coordX <<$>> byLine
    withLineCoord = fmap (\(n, l) -> Row n (tShow n : l)) withLineGapsFilled
    withColGapsFilled =
      fillGaps
        (T.intercalate " " (" ":replicate (fromIntegral width) "."))
        minY
        maxY
        rowNumber
        withLineCoord
    rez =
      T.intercalate " " (" " : (tShow <$> [minX .. maxX])) : withColGapsFilled


tShow :: Show a => a -> Text
tShow = T.pack . show

fillGaps ::
     (Integral b, Displayable c, Show b, Show c)
  => Text
  -> b
  -> b
  -> (c -> b)
  -> [c]
  -> [Text]
fillGaps filler from to extractFn = go [from..to]
  where
    go [] _ = []
    go js [] = filler <$ js
    go (j:js) second@(u:ss)
      | j < extractFn u = filler : go js second
      | j == extractFn u = label u : go js ss
      | otherwise = errorWithoutStackTrace $
        unlines [ "list was not sorted or has elements outside scope:"
                , unwords [ "\tgo"
                          , show (j:js)
                          , show second
                          ]
                ]

-- | similar to group but compares results of applying a function
-- and returns groups "labeled" with the application result
-- >>> labeledGroupOn snd [('a', 1), ('b', 1), ('c', 2)]
-- [([('a',1),('b',1)], 1), ([('c',2)], 2)]

labeledGroupOn :: Eq b => (a -> b) -> [a] -> [([a], b)]
labeledGroupOn _ [] = []
labeledGroupOn f (x:xs) = (x:ys, f x) : labeledGroupOn f zs
  where
    (ys, zs) = span ((f x ==) . f) xs

-- | like labeledGroupOn but puts the "label" first and list second
-- >>> labeledGroupOn1 snd [('a', 1), ('b', 1), ('c', 2)]
-- [(1, [('a',1),('b',1)]), (2, [('c',2)])]

labeledGroupOn1 :: Eq b => (a -> b) -> [a] -> [(b, [a])]
labeledGroupOn1 _ [] = []
labeledGroupOn1 f (x:xs) = (f x, x:ys) : labeledGroupOn1 f zs
  where
    (ys, zs) = span ((f x ==) . f) xs

(<<$>>) :: (Functor f, Functor g) => (a -> b) -> f (g a) -> f (g b)
(<<$>>) = fmap . fmap
infixl 4 <<$>>

(<<<$>>>) :: (Functor f, Functor g, Functor h) => (a -> b) -> f (g (h a)) -> f (g (h b))
(<<<$>>>) = fmap . fmap . fmap
infixl 4 <<<$>>>
